let USER_URL = "https://jsonplaceholder.typicode.com/users";
let tableBody = document.querySelector('#table-body');
let clickButton = document.querySelector('#click-button');
let modalXButton = document.querySelector(".close");
let modal = document.getElementById("myModal");
// let modalPost = document.querySelector("#modal-post");
let modalContent = document.querySelector('.modal-content')


const handleRowClick = async (id) => {
    removeModalPostElement();
    let posts = await fetchRecords(`${USER_URL}/${id}/posts`);
    console.log(posts);
    const modalPost = document.createElement('div');
    modalPost.setAttribute('id', 'modal-post');
    modalContent.appendChild(modalPost);

    posts.forEach(({title, body}) => {
        const containerDiv = document.createElement("div");
        const titleDiv = document.createElement("div");
        titleDiv.style.display = 'flex';
        titleDiv.style.flexDirection = 'column'
        titleDiv.style.alignItems = 'center'

        const h4 = document.createElement('h4');
        const newTitleContent = document.createTextNode(title);
        h4.appendChild(newTitleContent)
        titleDiv.appendChild(h4);

        const bodyDiv = document.createElement("div");
        bodyDiv.style.border = '1px solid black';
        bodyDiv.style.padding = '10px';
        const newBodyContent = document.createTextNode(body);
        bodyDiv.appendChild(newBodyContent)

        containerDiv.append(titleDiv, bodyDiv)
        modalPost.appendChild(containerDiv)
    })

    modal.style.display = "block";
}

const addRowsToTable = async () => {
    let table = document.querySelector("#myTable");
    let users = await fetchRecords(USER_URL);
    users.forEach(({id, email, name, username}) => {
        let row = table.insertRow(-1);
        row.addEventListener('click', () => handleRowClick(id))

        let cell1 = row.insertCell(0)
        cell1.innerHTML = id;
        let cell2 = row.insertCell(1)
        cell2.innerHTML = name;
        let cell3 = row.insertCell(2)
        cell3.innerHTML = username;
        let cell4 = row.insertCell(3)
        cell4.innerHTML = email
    })

}

const fetchRecords = (url) => {
    return fetch(url)
        .then(response => response.json())
        .then(data => data)
}

const hideModal = () => {
    modal.style.display = "none";
    removeModalPostElement()
}

const removeModalPostElement = () => {
    let modalPost = document.querySelector("#modal-post");
    if (modalPost) modalPost.remove();
}

modalXButton.addEventListener('click', hideModal);
window.addEventListener('load', addRowsToTable);
